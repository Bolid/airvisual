package ru.portfolio.authentication.exceptions

class PinCodeConfirmException : RuntimeException("Error confirmation pin code") {
}