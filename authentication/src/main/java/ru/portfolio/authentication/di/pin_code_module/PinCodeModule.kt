package ru.portfolio.authentication.di.pin_code_module

import dagger.Binds
import dagger.Module
import ru.portfolio.authentication.data.pin_code.PinCodeSetupRepository
import ru.portfolio.authentication.domain.pin_code.PinCodeSetupInteractor
import ru.portfolio.authentication.domain.pin_code.PinCodeSetupInteractorImpl
import ru.portfolio.authentication.domain.pin_code.PinCodeValidator
import ru.portfolio.authentication.domain.pin_code.PinCodeValidatorImpl
import ru.portfolio.authentication.repository.pin_code.PinCodeRepositoryImpl

@Module
abstract class PinCodeModule {

    @Binds
    abstract fun bindInteractor(fragment: PinCodeSetupInteractorImpl): PinCodeSetupInteractor

    @Binds
    abstract fun bindRepository(fragment: PinCodeRepositoryImpl): PinCodeSetupRepository

    @Binds
    abstract fun bindPinCodeValidator(validator: PinCodeValidatorImpl): PinCodeValidator
}