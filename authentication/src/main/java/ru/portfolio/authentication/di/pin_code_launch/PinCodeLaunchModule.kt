package ru.portfolio.authentication.di.pin_code_launch

import dagger.Binds
import dagger.Module
import ru.portfolio.authentication.di.pin_code_module.PinCodeModule
import ru.portfolio.authentication.presentation.launch.presenter.PinCodeLaunchPresenter
import ru.portfolio.authentication.presentation.launch.presenter.PinCodeSLaunchPresenterImpl
import ru.portfolio.authentication.presentation.launch.view.PinCodeLaunchView
import ru.portfolio.authentication.presentation.launch.view.PinLaunchFragment

@Module(includes = [PinCodeModule::class])
abstract class PinCodeLaunchModule {

    @Binds
    abstract fun bindView(fragment: PinLaunchFragment): PinCodeLaunchView

    @Binds
    abstract fun bindPresenter(fragment: PinCodeSLaunchPresenterImpl): PinCodeLaunchPresenter

}