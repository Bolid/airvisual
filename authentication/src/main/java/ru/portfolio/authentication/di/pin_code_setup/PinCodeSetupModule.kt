package ru.portfolio.authentication.di.pin_code_setup

import dagger.Binds
import dagger.Module
import ru.portfolio.authentication.di.pin_code_module.PinCodeModule
import ru.portfolio.authentication.presentation.pin_setup.presenter.PinCodeSetupPresenter
import ru.portfolio.authentication.presentation.pin_setup.presenter.PinCodeSetupPresenterImpl
import ru.portfolio.authentication.presentation.pin_setup.view.PinCodeSetupView
import ru.portfolio.authentication.presentation.pin_setup.view.PinSetupFragment

@Module(includes = [PinCodeModule::class])
abstract class PinCodeSetupModule {

    @Binds
    abstract fun bindView(fragment: PinSetupFragment): PinCodeSetupView

    @Binds
    abstract fun bindPresenter(fragment: PinCodeSetupPresenterImpl): PinCodeSetupPresenter

}