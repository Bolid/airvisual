package ru.portfolio.authentication.domain.pin_code

import ru.portfolio.authentication.data.pin_code.PinCodeSetupRepository
import ru.portfolio.authentication.exceptions.PinCodeConfirmException
import javax.inject.Inject

interface PinCodeSetupInteractor {
    fun setPinCode(pinCode: CharSequence): Boolean
    fun getPinCode(): String
}

class PinCodeSetupInteractorImpl @Inject constructor(
    private val validator: PinCodeValidator,
    private val repository: PinCodeSetupRepository
) :
    PinCodeSetupInteractor {
    override fun setPinCode(pinCode: CharSequence): Boolean {
        val str = validator.validationPin(pinCode)
        if (str.isNullOrEmpty()) return false
        repository.savePinCode(str)
        return true
    }

    override fun getPinCode(): String {
        return repository.getPinCode()
    }
}

interface PinCodeValidator {
    fun validationPin(pin: CharSequence): String?
}

class PinCodeValidatorImpl @Inject constructor() : PinCodeValidator {
    override fun validationPin(pin: CharSequence): String? {
        if (pin.length < 8) return null

        (0..3).forEach {
            if (pin[it] != pin[it + 4]) throw PinCodeConfirmException()
        }
        return pin.substring(0, 4)
    }
}