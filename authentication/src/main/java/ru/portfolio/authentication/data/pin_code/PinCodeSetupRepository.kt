package ru.portfolio.authentication.data.pin_code

interface PinCodeSetupRepository {
    fun savePinCode(pin: CharSequence)
    fun getPinCode(): String
}