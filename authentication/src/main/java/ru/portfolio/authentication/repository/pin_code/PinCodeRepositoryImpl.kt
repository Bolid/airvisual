package ru.portfolio.authentication.repository.pin_code

import android.content.SharedPreferences
import ru.portfolio.authentication.data.pin_code.PinCodeSetupRepository
import javax.inject.Inject

private const val KEY_PREFERENCE_PIN_CODE = "key_preference_pin_code"

class PinCodeRepositoryImpl @Inject constructor(private val sharedPreferences: SharedPreferences) :
    PinCodeSetupRepository {
    override fun savePinCode(pin: CharSequence) {
        sharedPreferences.edit().putString(KEY_PREFERENCE_PIN_CODE, pin.toString()).apply()
    }

    override fun getPinCode(): String {
        return sharedPreferences.getString(KEY_PREFERENCE_PIN_CODE, "") ?: ""
    }
}