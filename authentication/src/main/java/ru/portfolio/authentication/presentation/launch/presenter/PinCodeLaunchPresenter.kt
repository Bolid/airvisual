package ru.portfolio.authentication.presentation.launch.presenter

import ru.portfolio.authentication.domain.pin_code.PinCodeSetupInteractor
import ru.portfolio.authentication.presentation.launch.view.PinCodeLaunchView
import javax.inject.Inject

interface PinCodeLaunchPresenter {
    fun checkPin(pin: CharSequence?)
}

class PinCodeSLaunchPresenterImpl @Inject constructor(
    private val view: PinCodeLaunchView,
    private val interactor: PinCodeSetupInteractor
) :
    PinCodeLaunchPresenter {

    override fun checkPin(pin: CharSequence?) {
        if (pin?.length != 4) return
        if (pin.toString() == interactor.getPinCode())
            view.pinSuccess()
        else
            view.pinFail()
    }

}