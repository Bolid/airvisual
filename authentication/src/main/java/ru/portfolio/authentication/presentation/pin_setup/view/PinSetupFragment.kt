package ru.portfolio.authentication.presentation.pin_setup.view

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_pincode_setup.*
import ru.portfolio.authentication.R
import ru.portfolio.authentication.presentation.PinCodeInputCompleteListener
import ru.portfolio.authentication.presentation.PinCodeInputWatcher
import ru.portfolio.authentication.presentation.pin_setup.presenter.PinCodeSetupPresenter
import javax.inject.Inject

interface PinCodeSetupView {
    fun savePinComplete()
    fun pinCodeFailConfirm(error: String)
    fun existPinCode()
}

interface PinCodeSetupExitsListener {
    fun pinCodeSetupExist()
}

class PinSetupFragment : DaggerFragment(), PinCodeSetupView {

    @Inject
    lateinit var presenter: PinCodeSetupPresenter

    private var pinCodeSetupCompleteListener: PinCodeInputCompleteListener? = null

    private var pinCodeSetupExitsListener: PinCodeSetupExitsListener? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is PinCodeInputCompleteListener) pinCodeSetupCompleteListener = context
        if (context is PinCodeSetupExitsListener) pinCodeSetupExitsListener = context
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_pincode_setup, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initClicks()
        presenter.existPinCode()
        input_pin_confirm.addTextChangedListener(PinCodeInputWatcher {
            val stringBuilder = StringBuilder()
            stringBuilder.append(input_pin.text).append(it)
            presenter.savePin(stringBuilder)
        })
    }


    override fun onDetach() {
        super.onDetach()
        pinCodeSetupCompleteListener = null
        pinCodeSetupExitsListener = null
    }

    override fun savePinComplete() {
        pinCodeSetupCompleteListener?.pinCodeInputComplete()
    }

    override fun existPinCode() {
        pinCodeSetupExitsListener?.pinCodeSetupExist()
    }

    override fun pinCodeFailConfirm(error: String) {
        Toast.makeText(context, error, Toast.LENGTH_LONG).show()
    }

    private fun initClicks() {
        view?.findViewById<View>(R.id.button1)?.setOnClickListener { digitClick(1) }
        view?.findViewById<View>(R.id.button2)?.setOnClickListener { digitClick(2) }
        view?.findViewById<View>(R.id.button3)?.setOnClickListener { digitClick(3) }
        view?.findViewById<View>(R.id.button4)?.setOnClickListener { digitClick(4) }
        view?.findViewById<View>(R.id.button5)?.setOnClickListener { digitClick(5) }
        view?.findViewById<View>(R.id.button6)?.setOnClickListener { digitClick(6) }
        view?.findViewById<View>(R.id.button7)?.setOnClickListener { digitClick(7) }
        view?.findViewById<View>(R.id.button8)?.setOnClickListener { digitClick(8) }
        view?.findViewById<View>(R.id.button9)?.setOnClickListener { digitClick(9) }
        view?.findViewById<View>(R.id.button0)?.setOnClickListener { digitClick(0) }
        view?.findViewById<View>(R.id.buttonSpec)?.setOnClickListener { savePinComplete() }
        view?.findViewById<View>(R.id.buttonDelete)?.setOnClickListener {
            if (input_pin_confirm.length() != 0) {
                val text = input_pin_confirm.text
                if (text.isNullOrEmpty().not())
                    input_pin_confirm.text = text.substring(0, text.length - 1)
            } else {
                val text = input_pin.text
                if (text.isNullOrEmpty().not())
                    input_pin.text = text.substring(0, text.length - 1)
            }
        }
    }

    private fun digitClick(digit: Int) {
        if (input_pin.length() < 4)
            input_pin.append(digit.toString())
        else
            input_pin_confirm.append(digit.toString())
    }
}
