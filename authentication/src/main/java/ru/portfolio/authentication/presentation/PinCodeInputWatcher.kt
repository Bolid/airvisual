package ru.portfolio.authentication.presentation

import android.text.Editable
import android.text.TextWatcher

typealias WatcherEvent = (Editable?) -> Unit

class PinCodeInputWatcher(val watcherEvent: WatcherEvent) : TextWatcher {
    override fun afterTextChanged(p0: Editable?) {
        watcherEvent(p0)
    }

    override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }

    override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
    }
}