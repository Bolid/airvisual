package ru.portfolio.authentication.presentation.pin_setup.presenter

import ru.portfolio.authentication.domain.pin_code.PinCodeSetupInteractor
import ru.portfolio.authentication.exceptions.PinCodeConfirmException
import ru.portfolio.authentication.presentation.pin_setup.view.PinCodeSetupView
import javax.inject.Inject

interface PinCodeSetupPresenter {
    fun savePin(pin: CharSequence)
    fun existPinCode()
}

class PinCodeSetupPresenterImpl @Inject constructor(
    private val view: PinCodeSetupView,
    private val interactor: PinCodeSetupInteractor
) :
    PinCodeSetupPresenter {
    override fun savePin(pin: CharSequence) {
        try {
            if (interactor.setPinCode(pin))
                view.savePinComplete()
        } catch (e: PinCodeConfirmException) {
            view.pinCodeFailConfirm(e.message ?: "Error")
        }
    }

    override fun existPinCode() {
        if (interactor.getPinCode().isEmpty().not())
            view.existPinCode()
    }
}