package ru.portfolio.authentication.presentation.launch.view

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_pincode_launch.*
import ru.portfolio.authentication.R
import ru.portfolio.authentication.presentation.PinCodeInputCompleteListener
import ru.portfolio.authentication.presentation.PinCodeInputWatcher
import ru.portfolio.authentication.presentation.launch.presenter.PinCodeLaunchPresenter
import javax.inject.Inject

interface PinCodeLaunchView {
    fun pinSuccess()
    fun pinFail()
}

class PinLaunchFragment : DaggerFragment(), PinCodeLaunchView {

    @Inject
    lateinit var presenter: PinCodeLaunchPresenter

    private var pinCodeCompleteListener: PinCodeInputCompleteListener? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is PinCodeInputCompleteListener) pinCodeCompleteListener = context
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_pincode_launch, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initClicks()
        view.findViewById<View>(R.id.buttonSpec)?.visibility = View.GONE
        input_pin.addTextChangedListener(PinCodeInputWatcher { presenter.checkPin(it) })
    }

    override fun onDetach() {
        super.onDetach()
        pinCodeCompleteListener = null
    }

    override fun pinSuccess() {
        pinCodeCompleteListener?.pinCodeInputComplete()
    }

    override fun pinFail() {
        Toast.makeText(context, "Wrong PIN code entered", Toast.LENGTH_LONG).show()
    }

    private fun initClicks() {
        view?.findViewById<View>(R.id.button1)?.setOnClickListener { digitClick(1) }
        view?.findViewById<View>(R.id.button2)?.setOnClickListener { digitClick(2) }
        view?.findViewById<View>(R.id.button3)?.setOnClickListener { digitClick(3) }
        view?.findViewById<View>(R.id.button4)?.setOnClickListener { digitClick(4) }
        view?.findViewById<View>(R.id.button5)?.setOnClickListener { digitClick(5) }
        view?.findViewById<View>(R.id.button6)?.setOnClickListener { digitClick(6) }
        view?.findViewById<View>(R.id.button7)?.setOnClickListener { digitClick(7) }
        view?.findViewById<View>(R.id.button8)?.setOnClickListener { digitClick(8) }
        view?.findViewById<View>(R.id.button9)?.setOnClickListener { digitClick(9) }
        view?.findViewById<View>(R.id.button0)?.setOnClickListener { digitClick(0) }
        view?.findViewById<View>(R.id.buttonDelete)?.setOnClickListener {
            val text = input_pin.text
            if (text.isNullOrEmpty().not())
                input_pin.text = text.substring(0, text.length - 1)
        }
    }

    private fun digitClick(digit: Int) {
        input_pin.append(digit.toString())
    }

}
