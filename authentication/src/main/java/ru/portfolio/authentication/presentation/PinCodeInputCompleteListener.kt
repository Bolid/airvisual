package ru.portfolio.authentication.presentation

interface PinCodeInputCompleteListener {
    fun pinCodeInputComplete()
}