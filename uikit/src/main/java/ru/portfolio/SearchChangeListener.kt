package ru.portfolio

typealias SearchObserve = (String?) -> Unit

interface SearchChangeListener {
    fun onSearchChange(searchObserve: SearchObserve)
}

