package ru.portfolio.core.navigation

import android.os.Bundle

interface NavigationController {
    fun navigate(type: NavigationType, bundle: Bundle? = null)
}

enum class NavigationType {
    COUNTY,
    STATE,
    CITY,
    CITY_DETAILS
}