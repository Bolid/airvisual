package ru.portfolio.core.utils

import java.text.SimpleDateFormat
import java.util.*

private const val DATE_PATTERN = "dd MMM yyyy hh:mm"
private const val DATE_SERVER = "yyyy-MM-dd"

class DateUtils {
    companion object {
        fun dateFormat(dateString: String): String {
            val date = dateParse(dateString) ?: Date()
            return SimpleDateFormat(DATE_PATTERN, Locale.getDefault()).format(date)
        }

        private fun dateParse(dateString: String): Date? {
            return SimpleDateFormat(DATE_SERVER, Locale.getDefault()).parse(dateString)
        }
    }
}