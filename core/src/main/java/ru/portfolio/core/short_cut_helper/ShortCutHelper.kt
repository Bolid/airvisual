package ru.portfolio.core.short_cut_helper

interface ShortCutHelper {
    fun shortCutCreate(model: List<ShortCutModel>)
}

class ShortCutModel(
    val title: String,
    val longTitle: String,
    val iconRes: Int
)