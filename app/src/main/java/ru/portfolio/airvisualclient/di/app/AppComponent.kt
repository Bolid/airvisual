package ru.portfolio.airvisualclient.di.app

import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import ru.portfolio.airvisualclient.App
import ru.portfolio.core.di.OkHttpBuilderModule
import javax.inject.Singleton

@Component(
    modules = [
        AppModule::class,
        FragmentBuilder::class,
        OkHttpBuilderModule::class,
        ShortCutModule::class,
        AndroidSupportInjectionModule::class]
)
@Singleton
interface AppComponent : AndroidInjector<App> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: App): Builder

        fun build(): AppComponent
    }
}