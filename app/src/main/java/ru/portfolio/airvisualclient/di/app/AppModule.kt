package ru.portfolio.airvisualclient.di.app

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import dagger.Binds
import dagger.Module
import dagger.Provides
import ru.portfolio.airvisualclient.App
import ru.portfolio.airvisualclient.presentation.short_cut_helper.ShortCutHelperImpl
import ru.portfolio.core.short_cut_helper.ShortCutHelper

@Module
class AppModule {

    @Provides
    fun provideContext(application: App): Context = application.applicationContext

    @Provides
    fun provideSharedPreference(context: Context): SharedPreferences =
        PreferenceManager.getDefaultSharedPreferences(context)
}

@Module
abstract class ShortCutModule() {
    @Binds
    abstract fun bindsShortCut(shortCutHelperImpl: ShortCutHelperImpl): ShortCutHelper
}
