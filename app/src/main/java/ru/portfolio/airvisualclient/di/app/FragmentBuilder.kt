package ru.portfolio.airvisualclient.di.app

import dagger.Module
import dagger.android.ContributesAndroidInjector
import ru.portfolio.airvisual.di.AirVisualNetworkModule
import ru.portfolio.airvisual.di.city.AirVisualCityModule
import ru.portfolio.airvisual.di.city_details.AirVisualCityDetailsModule
import ru.portfolio.airvisual.di.city_details_by_ip.AirVisualCityDetailsIpModule
import ru.portfolio.airvisual.di.city_details_by_location.AirVisualCityDetailsLocationModule
import ru.portfolio.airvisual.di.city_detection.AirVisualCityDetectionModule
import ru.portfolio.airvisual.di.country.AirVisualCountryModule
import ru.portfolio.airvisual.di.state.AirVisualStateModule
import ru.portfolio.airvisual.presentation.city.view.AirVisualCityFragment
import ru.portfolio.airvisual.presentation.city_details.view.AirVisualCityDetailsFragment
import ru.portfolio.airvisual.presentation.country.view.AirVisualCountryFragment
import ru.portfolio.airvisual.presentation.detection_city.view.AirVisualDetectionCityFragment
import ru.portfolio.airvisual.presentation.detection_city_by_ip.view.AirVisualDetectionByIpFragment
import ru.portfolio.airvisual.presentation.detection_city_by_location.view.AirVisualDetectionByLocationFragment
import ru.portfolio.airvisual.presentation.state.view.AirVisualStateFragment
import ru.portfolio.authentication.di.pin_code_launch.PinCodeLaunchModule
import ru.portfolio.authentication.di.pin_code_setup.PinCodeSetupModule
import ru.portfolio.authentication.presentation.launch.view.PinLaunchFragment
import ru.portfolio.authentication.presentation.pin_setup.view.PinSetupFragment

@Module(includes = [AirVisualNetworkModule::class])
abstract class FragmentBuilder {
    @ContributesAndroidInjector(modules = [AirVisualCountryModule::class])
    abstract fun bindAirVisualCountryFragment(): AirVisualCountryFragment

    @ContributesAndroidInjector(modules = [AirVisualStateModule::class])
    abstract fun bindAirVisualStateFragment(): AirVisualStateFragment

    @ContributesAndroidInjector(modules = [AirVisualCityModule::class])
    abstract fun bindAirVisualCityFragment(): AirVisualCityFragment

    @ContributesAndroidInjector(modules = [AirVisualCityDetailsModule::class])
    abstract fun bindAirVisualCityDetailsFragment(): AirVisualCityDetailsFragment

    @ContributesAndroidInjector(modules = [AirVisualCityDetailsLocationModule::class])
    abstract fun bindAirVisualDetectionByLocationFragment(): AirVisualDetectionByLocationFragment

    @ContributesAndroidInjector(modules = [AirVisualCityDetailsIpModule::class])
    abstract fun bindAirVisualDetectionByIpFragment(): AirVisualDetectionByIpFragment

    @ContributesAndroidInjector(modules = [AirVisualCityDetectionModule::class])
    abstract fun bindAirVisualDetectionCityFragment(): AirVisualDetectionCityFragment

    @ContributesAndroidInjector(modules = [PinCodeSetupModule::class])
    abstract fun bindPinSetupFragment(): PinSetupFragment

    @ContributesAndroidInjector(modules = [PinCodeLaunchModule::class])
    abstract fun bindPinLaunchFragment(): PinLaunchFragment

}
