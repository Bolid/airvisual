package ru.portfolio.airvisualclient.presentation.short_cut_helper

import android.content.Context
import android.content.Intent
import android.content.pm.ShortcutInfo
import android.content.pm.ShortcutManager
import android.graphics.drawable.Icon
import android.net.Uri
import android.os.Build
import androidx.annotation.RequiresApi
import ru.portfolio.airvisualclient.presentation.authentication.presentation.view.AuthenticationActivity
import ru.portfolio.core.short_cut_helper.ShortCutHelper
import ru.portfolio.core.short_cut_helper.ShortCutModel
import java.util.*
import javax.inject.Inject

class ShortCutHelperImpl @Inject constructor(private val context: Context) : ShortCutHelper {
    @RequiresApi(Build.VERSION_CODES.N_MR1)
    override fun shortCutCreate(model: List<ShortCutModel>) {
        val shortcutManager = context.getSystemService<ShortcutManager>(ShortcutManager::class.java)
        val shortCuts = model.map {
            ShortcutInfo.Builder(context, UUID.randomUUID().toString())
                .setShortLabel(it.title)
                .setLongLabel(it.longTitle)
                .setIcon(Icon.createWithResource(context, it.iconRes))
                .setIntent(
                    Intent(
                        Intent.ACTION_MAIN,
                        Uri.EMPTY,
                        context,
                        AuthenticationActivity::class.java
                    ).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                )
                .build()
        }
        shortcutManager!!.dynamicShortcuts = shortCuts
    }
}