package ru.portfolio.airvisualclient.presentation.navigation.view

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import kotlinx.android.synthetic.main.activity_navigation.*
import ru.portfolio.SearchChangeListener
import ru.portfolio.SearchObserve
import ru.portfolio.airvisualclient.R
import ru.portfolio.core.navigation.NavigationController
import ru.portfolio.core.navigation.NavigationType


class MainActivity : AppCompatActivity(), NavigationController, SearchChangeListener {
    private lateinit var navController: NavController

    private var searchObserve: SearchObserve? = null
    private var searchView: SearchView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_navigation)
        navController = Navigation.findNavController(
            this,
            R.id.nav_fragment_bottom
        )
        NavigationUI.setupWithNavController(nav_view, navController)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        val mSearch: MenuItem = menu!!.findItem(R.id.search)
        searchView = mSearch.actionView as SearchView
        searchView?.queryHint = "Search"
        searchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(p0: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(p0: String?): Boolean {
                searchObserve?.let { it(p0) }
                return true
            }
        })
        return super.onCreateOptionsMenu(menu)
    }

    override fun onSearchChange(searchObserve: SearchObserve) {
        this.searchObserve = searchObserve
    }

    override fun navigate(type: NavigationType, bundle: Bundle?) {
        searchView?.onActionViewCollapsed()
        when (type) {
            NavigationType.STATE -> navController.navigate(
                R.id.action_item_air_to_airVisualStateFragment,
                bundle
            )
            NavigationType.CITY -> navController.navigate(
                R.id.action_airVisualStateFragment_to_airVisualCityFragment,
                bundle
            )
            NavigationType.CITY_DETAILS -> navController.navigate(
                R.id.action_airVisualCityFragment_to_airVisualCityDetailsFragment,
                bundle
            )
        }
    }
}