package ru.portfolio.airvisualclient.presentation.authentication.presentation.view

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.biometric.BiometricPrompt
import androidx.navigation.Navigation
import ru.portfolio.airvisualclient.R
import ru.portfolio.airvisualclient.presentation.navigation.view.MainActivity
import ru.portfolio.authentication.presentation.PinCodeInputCompleteListener
import ru.portfolio.authentication.presentation.pin_setup.view.PinCodeSetupExitsListener
import java.util.concurrent.Executors

class AuthenticationActivity : AppCompatActivity(), PinCodeInputCompleteListener,
    PinCodeSetupExitsListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_authentication)
        createBiometricPrompt()
    }

    override fun pinCodeInputComplete() {
        finish()
        val intent = Intent(this, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
    }

    override fun pinCodeSetupExist() {
        val controller = Navigation.findNavController(this, R.id.nav_fragment)
        val graph = controller.navInflater.inflate(R.navigation.nav_graph_authentication)
        graph.startDestination = R.id.pinLaunchFragment
        controller.graph = graph
    }


    private fun createBiometricPrompt() {
        val executor = Executors.newSingleThreadExecutor()
        val promt =
            BiometricPrompt(this, executor, object : BiometricPrompt.AuthenticationCallback() {
                override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
                    super.onAuthenticationSucceeded(result)
                    pinCodeInputComplete()
                }

                override fun onAuthenticationFailed() {
                    super.onAuthenticationFailed()
                    runOnUiThread {
                        Toast.makeText(
                            this@AuthenticationActivity,
                            "Fail scanning fingerprint",
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }

            })
        promt.authenticate(
            BiometricPrompt.PromptInfo.Builder()
                .setTitle("Fingerprint scan")
                .setNegativeButtonText("Cancel")
                .build()
        )
    }
}