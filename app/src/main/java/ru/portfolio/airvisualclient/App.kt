package ru.portfolio.airvisualclient

import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import ru.portfolio.airvisualclient.di.app.DaggerAppComponent

class App : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent
            .builder()
            .application(this)
            .build()
    }
}