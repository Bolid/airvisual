package ru.portfolio.airvisual.di.city_details

import dagger.Binds
import dagger.Module
import ru.portfolio.airvisual.data.city_details.AirVisualCityDetailsRepository
import ru.portfolio.airvisual.data.city_details.CityDetailsModelMapper
import ru.portfolio.airvisual.data.city_details.CityDetailsModelMapperImpl
import ru.portfolio.airvisual.data.city_detection_storage.CityDetectionSaveRepository
import ru.portfolio.airvisual.domain.city_details.AirVisualCityDetailsInteractor
import ru.portfolio.airvisual.domain.city_details.AirVisualCityDetailsInteractorImpl
import ru.portfolio.airvisual.domain.city_detection_storage.CityDetectionModel
import ru.portfolio.airvisual.domain.city_detection_storage.CityDetectionSaveInteractor
import ru.portfolio.airvisual.domain.city_detection_storage.CityDetectionSaveInteractorImpl
import ru.portfolio.airvisual.presentation.city_details.presenter.AirVisualCityDetailsMapper
import ru.portfolio.airvisual.presentation.city_details.presenter.AirVisualCityDetailsMapperImpl
import ru.portfolio.airvisual.presentation.city_details.presenter.AirVisualCityDetailsPresenter
import ru.portfolio.airvisual.presentation.city_details.presenter.AirVisualCityDetailsPresenterImpl
import ru.portfolio.airvisual.presentation.city_details.view.AirVisualCityDetailsFragment
import ru.portfolio.airvisual.presentation.city_details.view.AirVisualCityDetailsView
import ru.portfolio.airvisual.repository.city_details.AirVisualCityDetailsRepositoryImpl
import ru.portfolio.airvisual.repository.city_detection_storage.*

@Module
abstract class AirVisualCityDetailsModule {

    @Binds
    abstract fun bindsAirVisualView(view: AirVisualCityDetailsFragment): AirVisualCityDetailsView

    @Binds
    abstract fun bindsAirVisualMapper(mapper: AirVisualCityDetailsMapperImpl): AirVisualCityDetailsMapper

    @Binds
    abstract fun bindsPresenter(presenter: AirVisualCityDetailsPresenterImpl): AirVisualCityDetailsPresenter

    @Binds
    abstract fun bindsCityDetectionSaveInteractorImpl(cityDetectionSaveInteractorImpl: CityDetectionSaveInteractorImpl): CityDetectionSaveInteractor

    @Binds
    abstract fun bindsGsonConverterData(gsonConverterData: GsonConverterData): ConverterData<CityDetectionModel>

    @Binds
    abstract fun bindsInteractor(interactorAirVisual: AirVisualCityDetailsInteractorImpl): AirVisualCityDetailsInteractor

    @Binds
    abstract fun bindsRepository(repositoryImpl: AirVisualCityDetailsRepositoryImpl): AirVisualCityDetailsRepository

    @Binds
    abstract fun bindsCityDetectionSharedRepositoryImpl(repositoryImpl: CityDetectionDelegate): CityDetectionSaveRepository

    @Binds
    abstract fun bindsCityDetailsModelMapper(repositoryImpl: CityDetailsModelMapperImpl): CityDetailsModelMapper

    @Binds
    abstract fun bindsObservableDetection(repositoryImpl: ObservableCityDetection): ObservableDetection<CityDetectionModel>


}