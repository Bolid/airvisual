package ru.portfolio.airvisual.di.city

import dagger.Binds
import dagger.Module
import ru.portfolio.airvisual.data.city.AirVisualCityRepository
import ru.portfolio.airvisual.domain.city.AirVisualCityInteractor
import ru.portfolio.airvisual.domain.city.AirVisualCityInteractorImpl
import ru.portfolio.airvisual.presentation.base.view.AirVisualView
import ru.portfolio.airvisual.presentation.city.presenter.AirVisualCityPresenter
import ru.portfolio.airvisual.presentation.city.presenter.AirVisualCityPresenterImpl
import ru.portfolio.airvisual.presentation.city.view.AirVisualCityFragment
import ru.portfolio.airvisual.repository.city.AirVisualCityRepositoryImpl

@Module
abstract class AirVisualCityModule {

    @Binds
    abstract fun bindsAirVisualView(view: AirVisualCityFragment): AirVisualView

    @Binds
    abstract fun bindsPresenter(presenter: AirVisualCityPresenterImpl): AirVisualCityPresenter

    @Binds
    abstract fun bindsInteractor(interactorAirVisual: AirVisualCityInteractorImpl): AirVisualCityInteractor

    @Binds
    abstract fun bindsRepository(repositoryImpl: AirVisualCityRepositoryImpl): AirVisualCityRepository

}