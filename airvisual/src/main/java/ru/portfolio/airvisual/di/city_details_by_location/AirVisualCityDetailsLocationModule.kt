package ru.portfolio.airvisual.di.city_details_by_location

import dagger.Binds
import dagger.Module
import ru.portfolio.airvisual.data.city_by_location.AirVisualCityDetailsLocationRepository
import ru.portfolio.airvisual.data.city_details.CityDetailsModelMapper
import ru.portfolio.airvisual.data.city_details.CityDetailsModelMapperImpl
import ru.portfolio.airvisual.domain.city_detection_location.AirVisualCityLocationInteractor
import ru.portfolio.airvisual.domain.city_detection_location.AirVisualCityLocationInteractorImpl
import ru.portfolio.airvisual.presentation.city_details.presenter.AirVisualCityDetailsMapper
import ru.portfolio.airvisual.presentation.city_details.presenter.AirVisualCityDetailsMapperImpl
import ru.portfolio.airvisual.presentation.detection_city_by_location.presenter.AirVisualDetectionByLocationPresenter
import ru.portfolio.airvisual.presentation.detection_city_by_location.presenter.AirVisualDetectionByLocationPresenterImpl
import ru.portfolio.airvisual.presentation.detection_city_by_location.view.AirVisualCityLocationView
import ru.portfolio.airvisual.presentation.detection_city_by_location.view.AirVisualDetectionByLocationFragment
import ru.portfolio.airvisual.repository.city_by_location.AirVisualCityDetailsLocationRepositoryImpl

@Module
abstract class AirVisualCityDetailsLocationModule {

    @Binds
    abstract fun bindsAirVisualView(view: AirVisualDetectionByLocationFragment): AirVisualCityLocationView

    @Binds
    abstract fun bindsAirVisualMapper(mapper: AirVisualCityDetailsMapperImpl): AirVisualCityDetailsMapper

    @Binds
    abstract fun bindsPresenter(presenter: AirVisualDetectionByLocationPresenterImpl): AirVisualDetectionByLocationPresenter

    @Binds
    abstract fun bindsInteractor(interactorAirVisual: AirVisualCityLocationInteractorImpl): AirVisualCityLocationInteractor

    @Binds
    abstract fun bindsRepository(repositoryImpl: AirVisualCityDetailsLocationRepositoryImpl): AirVisualCityDetailsLocationRepository

    @Binds
    abstract fun bindsCityDetailsModelMapper(repositoryImpl: CityDetailsModelMapperImpl): CityDetailsModelMapper

}