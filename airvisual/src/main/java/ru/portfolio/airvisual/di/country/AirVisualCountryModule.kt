package ru.portfolio.airvisual.di.country

import dagger.Binds
import dagger.Module
import ru.portfolio.airvisual.data.country.AirVisualCountryRepository
import ru.portfolio.airvisual.domain.country.AirVisualCountryInteractor
import ru.portfolio.airvisual.domain.country.AirVisualCountryInteractorImpl
import ru.portfolio.airvisual.presentation.base.view.AirVisualView
import ru.portfolio.airvisual.presentation.country.presenter.AirVisualCountryPresenterImpl
import ru.portfolio.airvisual.presentation.country.presenter.AirVisualPresenter
import ru.portfolio.airvisual.presentation.country.view.AirVisualCountryFragment
import ru.portfolio.airvisual.repository.country.AirVisualCountryRepositoryImpl

@Module
abstract class AirVisualCountryModule {
    @Binds
    abstract fun bindsAirVisualView(view: AirVisualCountryFragment): AirVisualView

    @Binds
    abstract fun bindsPresenter(presenter: AirVisualCountryPresenterImpl): AirVisualPresenter

    @Binds
    abstract fun bindsInteractor(interactor: AirVisualCountryInteractorImpl): AirVisualCountryInteractor

    @Binds
    abstract fun bindsRepository(countryRepository: AirVisualCountryRepositoryImpl): AirVisualCountryRepository

}