package ru.portfolio.airvisual.di.city_details_by_ip

import dagger.Binds
import dagger.Module
import ru.portfolio.airvisual.data.city_by_ip.AirVisualCityDetailsIpRepository
import ru.portfolio.airvisual.data.city_details.CityDetailsModelMapper
import ru.portfolio.airvisual.data.city_details.CityDetailsModelMapperImpl
import ru.portfolio.airvisual.domain.city_details_by_ip.AirVisualCityDetailsByIpInteractor
import ru.portfolio.airvisual.domain.city_details_by_ip.AirVisualCityDetailsByIpInteractorImpl
import ru.portfolio.airvisual.presentation.city_details.presenter.AirVisualCityDetailsMapper
import ru.portfolio.airvisual.presentation.city_details.presenter.AirVisualCityDetailsMapperImpl
import ru.portfolio.airvisual.presentation.detection_city_by_ip.presenter.AirVisualCityDetailsByIpPresenter
import ru.portfolio.airvisual.presentation.detection_city_by_ip.presenter.AirVisualCityDetailsByIpPresenterImpl
import ru.portfolio.airvisual.presentation.detection_city_by_ip.view.AirVisualDetectionByIpFragment
import ru.portfolio.airvisual.presentation.detection_city_by_location.view.AirVisualCityLocationView
import ru.portfolio.airvisual.repository.city_details_by_ip.AirVisualCityDetailsIpRepositoryImpl

@Module
abstract class AirVisualCityDetailsIpModule {

    @Binds
    abstract fun bindsAirVisualView(view: AirVisualDetectionByIpFragment): AirVisualCityLocationView

    @Binds
    abstract fun bindsAirVisualMapper(mapper: AirVisualCityDetailsMapperImpl): AirVisualCityDetailsMapper

    @Binds
    abstract fun bindsPresenter(presenter: AirVisualCityDetailsByIpPresenterImpl): AirVisualCityDetailsByIpPresenter

    @Binds
    abstract fun bindsInteractor(interactorAirVisual: AirVisualCityDetailsByIpInteractorImpl): AirVisualCityDetailsByIpInteractor

    @Binds
    abstract fun bindsRepository(repositoryImpl: AirVisualCityDetailsIpRepositoryImpl): AirVisualCityDetailsIpRepository

    @Binds
    abstract fun bindsCityDetailsModelMapper(repositoryImpl: CityDetailsModelMapperImpl): CityDetailsModelMapper

}