package ru.portfolio.airvisual.di.city_detection

import dagger.Binds
import dagger.Module
import ru.portfolio.airvisual.data.city_detection_storage.CityDetectionReadRepository
import ru.portfolio.airvisual.domain.city_detection_storage.CityDetectionModel
import ru.portfolio.airvisual.domain.city_detection_storage.CityDetectionReadInteractor
import ru.portfolio.airvisual.domain.city_detection_storage.CityDetectionReadInteractorImpl
import ru.portfolio.airvisual.presentation.detection_city.presenter.AirCityDetectionPresenter
import ru.portfolio.airvisual.presentation.detection_city.presenter.AirCityDetectionPresenterImpl
import ru.portfolio.airvisual.presentation.detection_city.view.AirVisualDetectionCityFragment
import ru.portfolio.airvisual.presentation.detection_city.view.AirVisualDetectionCityView
import ru.portfolio.airvisual.repository.city_detection_storage.CityDetectionSharedSaveRepositoryImpl
import ru.portfolio.airvisual.repository.city_detection_storage.ConverterData
import ru.portfolio.airvisual.repository.city_detection_storage.GsonConverterData

@Module
abstract class AirVisualCityDetectionModule {

    @Binds
    abstract fun bindsAirVisualView(view: AirVisualDetectionCityFragment): AirVisualDetectionCityView

    @Binds
    abstract fun bindsAirVisualMapper(mapper: AirCityDetectionPresenterImpl): AirCityDetectionPresenter

    @Binds
    abstract fun bindsPresenter(presenter: CityDetectionReadInteractorImpl): CityDetectionReadInteractor

    @Binds
    abstract fun bindsRepository(repositoryImpl: CityDetectionSharedSaveRepositoryImpl): CityDetectionReadRepository

    @Binds
    abstract fun bindsGsonConverterData(gsonConverterData: GsonConverterData): ConverterData<CityDetectionModel>
}