package ru.portfolio.airvisual.di

import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import ru.portfolio.airvisual.BuildConfig
import ru.portfolio.airvisual.data.api.AirVisualApi
import javax.inject.Singleton

@Module
class AirVisualNetworkModule {
    @Provides
    @Singleton
    fun provideAirVisualApi(okHttpClient: OkHttpClient.Builder): AirVisualApi {
        return Retrofit.Builder()
            .client(okHttpClient.build())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BuildConfig.URL_AIR_VISUAL)
            .build().create(AirVisualApi::class.java)
    }
}

