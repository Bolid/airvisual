package ru.portfolio.airvisual.di.state

import dagger.Binds
import dagger.Module
import ru.portfolio.airvisual.data.state.AirVisualStateRepository
import ru.portfolio.airvisual.domain.state.AirVisualAirVisualStateInteractorImpl
import ru.portfolio.airvisual.domain.state.AirVisualStateInteractor
import ru.portfolio.airvisual.presentation.base.view.AirVisualView
import ru.portfolio.airvisual.presentation.state.presenter.AirVisualStatePresenter
import ru.portfolio.airvisual.presentation.state.presenter.AirVisualStatePresenterImpl
import ru.portfolio.airvisual.presentation.state.view.AirVisualStateFragment
import ru.portfolio.airvisual.repository.state.AirVisualStateRepositoryImpl

@Module
abstract class AirVisualStateModule {

    @Binds
    abstract fun bindsAirVisualView(view: AirVisualStateFragment): AirVisualView

    @Binds
    abstract fun bindsPresenter(presenter: AirVisualStatePresenterImpl): AirVisualStatePresenter

    @Binds
    abstract fun bindsInteractor(interactorAirVisual: AirVisualAirVisualStateInteractorImpl): AirVisualStateInteractor

    @Binds
    abstract fun bindsRepository(repositoryImpl: AirVisualStateRepositoryImpl): AirVisualStateRepository

}