package ru.portfolio.airvisual.data.city_detection_storage

import ru.portfolio.airvisual.domain.city_detection_storage.CityDetectionModel

interface CityDetectionSaveRepository {
    fun saveCity(model: CityDetectionModel)
}

interface CityDetectionReadRepository {
    fun getSavedCity(): List<CityDetectionModel>?
}