package ru.portfolio.airvisual.data.city_by_ip

import io.reactivex.Single
import ru.portfolio.airvisual.data.city_details.CityDetailsModel

interface AirVisualCityDetailsIpRepository {
    fun loadCityDetails(): Single<CityDetailsModel>
}