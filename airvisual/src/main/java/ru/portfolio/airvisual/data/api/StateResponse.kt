package ru.portfolio.airvisual.data.api

import com.google.gson.annotations.SerializedName

class StateListResponse(
    @SerializedName("data") val data: List<StateResponse>
)

class StateResponse(
    @SerializedName("state") val state: String
)