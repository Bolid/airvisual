package ru.portfolio.airvisual.data.country

import io.reactivex.Single

interface AirVisualCountryRepository {
    fun loadCountryList(search: String?): Single<List<String>>
}