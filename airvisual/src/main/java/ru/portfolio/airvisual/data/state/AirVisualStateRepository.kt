package ru.portfolio.airvisual.data.state

import io.reactivex.Single

interface AirVisualStateRepository {
    fun loadStateList(country: String, search: String?): Single<List<String>>
}