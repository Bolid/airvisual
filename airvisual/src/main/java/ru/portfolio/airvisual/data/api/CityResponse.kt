package ru.portfolio.airvisual.data.api

import com.google.gson.annotations.SerializedName

class CityListResponse(
    @SerializedName("data") val data: List<CityResponse>
)

class CityResponse(
    @SerializedName("city") val city: String
)