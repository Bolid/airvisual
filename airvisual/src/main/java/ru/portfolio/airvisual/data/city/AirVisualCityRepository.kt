package ru.portfolio.airvisual.data.city

import io.reactivex.Single

interface AirVisualCityRepository {
    fun loadCityList(country: String, state: String, search: String?): Single<List<String>>
}