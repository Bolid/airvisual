package ru.portfolio.airvisual.data.city_details

import ru.portfolio.airvisual.data.api.CityDetailsResponse
import javax.inject.Inject

class CityDetailsModel(
    val city: String,
    val state: String,
    val country: String,
    val dateWeather: String,
    val datePollution: String,
    val temperature: Int,
    val wing: Int,
    val pressure: Int,
    val aqius: Int,
    val mainus: String,
    val aqicn: Int
)

interface CityDetailsModelMapper {
    fun mapToCityDetailsModel(model: CityDetailsResponse): CityDetailsModel
}

class CityDetailsModelMapperImpl @Inject constructor() : CityDetailsModelMapper {
    override fun mapToCityDetailsModel(model: CityDetailsResponse): CityDetailsModel {
        return CityDetailsModel(
            city = model.cityDetailsData.city,
            state = model.cityDetailsData.state,
            country = model.cityDetailsData.country,
            dateWeather = model.cityDetailsData.currentData.weather.date,
            datePollution = model.cityDetailsData.currentData.pollution.date,
            temperature = model.cityDetailsData.currentData.weather.temperature,
            wing = model.cityDetailsData.currentData.weather.wing,
            pressure = model.cityDetailsData.currentData.weather.pressure,
            aqius = model.cityDetailsData.currentData.pollution.aqius,
            mainus = model.cityDetailsData.currentData.pollution.mainus,
            aqicn = model.cityDetailsData.currentData.pollution.aqicn
        )
    }
}