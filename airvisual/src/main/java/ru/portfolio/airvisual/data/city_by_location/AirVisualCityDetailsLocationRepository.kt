package ru.portfolio.airvisual.data.city_by_location

import io.reactivex.Single
import ru.portfolio.airvisual.data.city_details.CityDetailsModel

interface AirVisualCityDetailsLocationRepository {
    fun loadCityDetails(latitude: Double, longitude: Double): Single<CityDetailsModel>
}