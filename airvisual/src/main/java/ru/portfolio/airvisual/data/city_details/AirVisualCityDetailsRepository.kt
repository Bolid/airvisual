package ru.portfolio.airvisual.data.city_details

import io.reactivex.Single

interface AirVisualCityDetailsRepository {
    fun loadCityDetailsList(country: String, state: String, city: String): Single<CityDetailsModel>
}