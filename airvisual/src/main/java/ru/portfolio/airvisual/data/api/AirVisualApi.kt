package ru.portfolio.airvisual.data.api

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface AirVisualApi {
    @GET("/v2/countries")
    fun getCountryList(@Query("key") key: String): Single<CountryListResponse>

    @GET("/v2/states")
    fun getStateList(
        @Query("key") key: String,
        @Query("country") country: String
    ): Single<StateListResponse>

    @GET("/v2/cities")
    fun getCitiesList(
        @Query("key") key: String,
        @Query("state") state: String,
        @Query("country") country: String
    ): Single<CityListResponse>

    @GET("/v2/city")
    fun getCityDetails(
        @Query("key") key: String,
        @Query("state") state: String,
        @Query("country") country: String,
        @Query("city") city: String
    ): Single<CityDetailsResponse>

    @GET("/v2/nearest_city")
    fun getCityDetailsByLocation(
        @Query("key") key: String,
        @Query("lat") lat: Double,
        @Query("lon") lon: Double
    ): Single<CityDetailsResponse>


    @GET("/v2/nearest_city")
    fun getCityDetailsByIp(
        @Query("key") key: String
    ): Single<CityDetailsResponse>

}