package ru.portfolio.airvisual.data.api

import com.google.gson.annotations.SerializedName

class CountryListResponse(
    @SerializedName("data") val data: List<CountryResponse>
)

class CountryResponse(
    @SerializedName("country") val country: String
)