package ru.portfolio.airvisual.data.api

import com.google.gson.annotations.SerializedName

class CityDetailsResponse(
    @SerializedName("data") val cityDetailsData: CityDetailsData
)

class CityDetailsData(
    @SerializedName("city") val city: String,
    @SerializedName("state") val state: String,
    @SerializedName("country") val country: String,
    @SerializedName("current") val currentData: CityDetailsCurrent
)

class CityDetailsCurrent(
    @SerializedName("weather") val weather: CityDetailsWeather,
    @SerializedName("pollution") val pollution: CityDetailsPollution
)

class CityDetailsWeather(
    @SerializedName("ts") val date: String,
    @SerializedName("tp") val temperature: Int,
    @SerializedName("pr") val pressure: Int,
    @SerializedName("wing") val wing: Int
)

class CityDetailsPollution(
    @SerializedName("ts") val date: String,
    @SerializedName("aqius") val aqius: Int,
    @SerializedName("mainus") val mainus: String,
    @SerializedName("aqicn") val aqicn: Int
)