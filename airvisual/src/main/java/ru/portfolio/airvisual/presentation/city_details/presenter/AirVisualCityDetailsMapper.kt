package ru.portfolio.airvisual.presentation.city_details.presenter

import ru.portfolio.airvisual.data.city_details.CityDetailsModel
import ru.portfolio.airvisual.presentation.city_details.view.AirVisualCityDetailsViewState
import ru.portfolio.airvisual.presentation.city_details.view.AirVisualPollution
import ru.portfolio.airvisual.presentation.city_details.view.AirVisualWeather
import ru.portfolio.core.utils.DateUtils
import javax.inject.Inject

interface AirVisualCityDetailsMapper {
    fun mapToViewState(model: CityDetailsModel): AirVisualCityDetailsViewState
}

class AirVisualCityDetailsMapperImpl @Inject constructor() : AirVisualCityDetailsMapper {
    override fun mapToViewState(model: CityDetailsModel): AirVisualCityDetailsViewState {
        return AirVisualCityDetailsViewState(
            city = model.city,
            state = model.state,
            country = model.country,
            weather = AirVisualWeather(
                dateWeather = DateUtils.dateFormat(model.dateWeather),
                temperature = model.temperature.toString(),
                pressure = model.pressure.toString(),
                wind = model.wing.toString()
            ),
            pollution = AirVisualPollution(
                datePollution = DateUtils.dateFormat(model.datePollution),
                aqius = model.aqius.toString(),
                mainus = model.mainus,
                aqicn = model.aqicn.toString()
            )
        )
    }

}