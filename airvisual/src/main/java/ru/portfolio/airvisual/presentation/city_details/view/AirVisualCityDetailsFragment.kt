package ru.portfolio.airvisual.presentation.city_details.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_city_details.*
import ru.portfolio.airvisual.R
import ru.portfolio.airvisual.presentation.city.view.ARGUMENT_VALUE_STATE
import ru.portfolio.airvisual.presentation.city_details.presenter.AirVisualCityDetailsPresenter
import ru.portfolio.airvisual.presentation.state.view.ARGUMENT_VALUE_COUNTRY
import ru.portfolio.core.extension.share
import javax.inject.Inject

interface AirVisualCityDetailsView {
    fun showWeather(model: AirVisualWeather)
    fun showPollution(model: AirVisualPollution)
    fun shareStr(shareStr: String)
    fun sharingActivate(model: AirVisualCityDetailsViewState)
}


const val ARGUMENT_VALUE_CITY = "argument_value_city"

class AirVisualCityDetailsFragment : DaggerFragment(), AirVisualCityDetailsView {

    @Inject
    lateinit var presenter: AirVisualCityDetailsPresenter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_city_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.loadCityDetails(pullCountryFromArg(), pullStateFromArg(), pullCityFromArg())
        btn_detection.setOnClickListener {
            presenter.saveForDetection(
                pullCountryFromArg(),
                pullStateFromArg(),
                pullCityFromArg()
            )
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.clear()
    }

    override fun sharingActivate(model: AirVisualCityDetailsViewState) {
        btn_share.setOnClickListener { presenter.combineForShare(model) }
    }

    override fun showWeather(model: AirVisualWeather) {
        date_weather.text = model.dateWeather
        temperature.text = model.temperature
        wind.text = model.wind
        pressure.text = model.pressure
    }

    override fun showPollution(model: AirVisualPollution) {
        date_pollution.text = model.datePollution
        aqicn.text = model.aqicn
        mainus.text = model.mainus
        aqius.text = model.aqius
    }

    override fun shareStr(shareStr: String) {
        share(shareStr)
    }

    private fun pullStateFromArg() = arguments?.getString(ARGUMENT_VALUE_STATE) ?: ""
    private fun pullCountryFromArg() = arguments?.getString(ARGUMENT_VALUE_COUNTRY) ?: ""
    private fun pullCityFromArg() = arguments?.getString(ARGUMENT_VALUE_CITY) ?: ""
}