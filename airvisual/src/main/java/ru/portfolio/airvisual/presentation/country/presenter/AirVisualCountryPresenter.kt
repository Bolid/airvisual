package ru.portfolio.airvisual.presentation.country.presenter

import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.portfolio.airvisual.domain.country.AirVisualCountryInteractor
import ru.portfolio.airvisual.presentation.base.presenter.AirVisualBasePresenterImpl
import ru.portfolio.airvisual.presentation.base.view.AirVisualView
import javax.inject.Inject

interface AirVisualPresenter {
    fun loadCountyList()
    fun search(search: String? = null)
}

class AirVisualCountryPresenterImpl @Inject constructor(
    view: AirVisualView,
    private val interactor: AirVisualCountryInteractor
) : AirVisualBasePresenterImpl(view),
    AirVisualPresenter {

    override fun search(search: String?) {
        compositeDisposable.add(
            getLoadingThread(search)
                .subscribe(::handleSearchingData, ::handleError)
        )
    }

    override fun loadCountyList() {
        compositeDisposable.add(
            getLoadingThread()
                .subscribe(::handleSuccess, ::handleError)
        )
    }

    private fun getLoadingThread(search: String? = null): Single<List<String>> {
        return interactor.loadCountryList(search)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

}
