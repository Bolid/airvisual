package ru.portfolio.airvisual.presentation.base.presenter

import ru.portfolio.airvisual.presentation.BasePresenter
import ru.portfolio.airvisual.presentation.BasePresenterImpl
import ru.portfolio.airvisual.presentation.base.view.AirVisualView

interface AirVisualBasePresenter : BasePresenter {
}

abstract class AirVisualBasePresenterImpl(private val view: AirVisualView) : BasePresenterImpl(),
    AirVisualBasePresenter {

    protected fun handleSuccess(list: List<String>) {
        view.showResultList(list)
    }

    protected fun handleSearchingData(list: List<String>) {
        view.showSearchingData(list)
    }

    protected fun handleError(error: Throwable) {
        view.showError()
    }
}