package ru.portfolio.airvisual.presentation.detection_city_by_location.view

import android.content.Context
import android.content.pm.PackageManager
import android.graphics.drawable.Drawable
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import ru.portfolio.airvisual.R
import ru.portfolio.airvisual.presentation.detection_city.view.BaseCityDetectionIndicatorFragment
import ru.portfolio.airvisual.presentation.detection_city_by_location.presenter.AirVisualDetectionByLocationPresenter
import ru.portfolio.core.extension.checkLocationPermission
import javax.inject.Inject

private const val LOCATION_CODE = 123

interface AirVisualCityLocationView {
    fun showCity(cityValue: String)
    fun showPollution(pollutionValue: String)
    fun showProgress()
    fun hideProgress()
}

class AirVisualDetectionByLocationFragment : BaseCityDetectionIndicatorFragment() {

    @Inject
    lateinit var presenter: AirVisualDetectionByLocationPresenter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        updateLocation()
    }


    override fun onDestroyView() {
        super.onDestroyView()
        presenter.clear()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>, grantResults: IntArray
    ) {
        when (requestCode) {
            LOCATION_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    updateLocation()
                }
            }
        }
    }

    override fun setDetectionIcon(icon: Drawable?) {
        super.setDetectionIcon(
            ContextCompat.getDrawable(
                context!!,
                R.drawable.ic_baseline_computer_24
            )
        )
    }

    private fun updateLocation() {
        if (checkLocationPermission(LOCATION_CODE).not()) return
        val locationManager =
            context!!.getSystemService(Context.LOCATION_SERVICE) as LocationManager?
        showProgress()
        locationManager?.requestLocationUpdates(
            LocationManager.GPS_PROVIDER,
            1000,
            1000f,
            object : LocationListener {
                override fun onLocationChanged(location: Location) {
                    presenter.loadCityByLocation(location.latitude, location.longitude)
                }

                override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {

                }

                override fun onProviderEnabled(provider: String) {
                }

                override fun onProviderDisabled(provider: String) {

                }
            })
    }
}