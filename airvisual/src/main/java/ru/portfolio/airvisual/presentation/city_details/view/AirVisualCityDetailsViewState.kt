package ru.portfolio.airvisual.presentation.city_details.view

class AirVisualCityDetailsViewState(
    val city: String,
    val state: String,
    val country: String,
    val weather: AirVisualWeather,
    val pollution: AirVisualPollution
)

class AirVisualWeather(
    val dateWeather: String,
    val temperature: String,
    val pressure: String,
    val wind: String
)

class AirVisualPollution(
    val datePollution: String,
    val aqius: String,
    val mainus: String,
    val aqicn: String
)