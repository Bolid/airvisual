package ru.portfolio.airvisual.presentation.country.view

import android.os.Bundle
import android.view.View
import ru.portfolio.airvisual.presentation.base.view.AirVisualListFragment
import ru.portfolio.airvisual.presentation.country.presenter.AirVisualPresenter
import ru.portfolio.airvisual.presentation.state.view.ARGUMENT_VALUE_COUNTRY
import ru.portfolio.core.navigation.NavigationType
import javax.inject.Inject

class AirVisualCountryFragment : AirVisualListFragment() {

    @Inject
    lateinit var presenter: AirVisualPresenter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.loadCountyList()
    }

    override fun nextStep(value: String) {
        navigationController?.navigate(
            NavigationType.STATE,
            Bundle().apply { putString(ARGUMENT_VALUE_COUNTRY, value) })
    }

    override fun search(search: String?) {
        presenter.search(search)
    }
}


