package ru.portfolio.airvisual.presentation.detection_city.view

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_detection_indicator.*
import ru.portfolio.airvisual.R
import ru.portfolio.airvisual.presentation.detection_city_by_location.view.AirVisualCityLocationView

abstract class BaseCityDetectionIndicatorFragment : DaggerFragment(),
    AirVisualCityLocationView {

    final override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_detection_indicator, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setDetectionIcon(
            ContextCompat.getDrawable(
                context!!,
                R.drawable.ic_baseline_location_on_24
            )
        )
    }

    override fun showCity(cityValue: String) {
        city.text = cityValue
    }

    override fun showPollution(pollutionValue: String) {
        pollution.text = pollutionValue
    }

    override fun showProgress() {
        progressBar.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progressBar.visibility = View.GONE
    }

    protected open fun setDetectionIcon(icon: Drawable?) {
        detection_icon.setImageDrawable(icon)
    }


}