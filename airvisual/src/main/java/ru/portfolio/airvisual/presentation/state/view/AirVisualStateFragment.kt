package ru.portfolio.airvisual.presentation.state.view

import android.os.Bundle
import android.view.View
import ru.portfolio.airvisual.presentation.base.view.AirVisualListFragment
import ru.portfolio.airvisual.presentation.city.view.ARGUMENT_VALUE_STATE
import ru.portfolio.airvisual.presentation.state.presenter.AirVisualStatePresenter
import ru.portfolio.core.navigation.NavigationType
import javax.inject.Inject

const val ARGUMENT_VALUE_COUNTRY = "argument_value_country"

class AirVisualStateFragment : AirVisualListFragment() {

    @Inject
    lateinit var presenter: AirVisualStatePresenter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.loadStateList(pullCountryFromArg())
    }

    override fun nextStep(value: String) {
        navigationController?.navigate(
            NavigationType.CITY,
            Bundle().apply {
                putString(ARGUMENT_VALUE_COUNTRY, pullCountryFromArg())
                putString(ARGUMENT_VALUE_STATE, value)
            }
        )
    }

    override fun search(search: String?) {
        presenter.search(pullCountryFromArg(), search)
    }

    private fun pullCountryFromArg() = arguments?.getString(ARGUMENT_VALUE_COUNTRY) ?: ""
}




