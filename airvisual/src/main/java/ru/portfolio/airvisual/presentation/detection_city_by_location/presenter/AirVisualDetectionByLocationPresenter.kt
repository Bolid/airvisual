package ru.portfolio.airvisual.presentation.detection_city_by_location.presenter

import android.content.Context
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.portfolio.airvisual.R
import ru.portfolio.airvisual.domain.city_detection_location.AirVisualCityLocationInteractor
import ru.portfolio.airvisual.presentation.BasePresenter
import ru.portfolio.airvisual.presentation.BasePresenterImpl
import ru.portfolio.airvisual.presentation.city_details.presenter.AirVisualCityDetailsMapper
import ru.portfolio.airvisual.presentation.city_details.view.AirVisualCityDetailsViewState
import ru.portfolio.airvisual.presentation.detection_city_by_location.view.AirVisualCityLocationView
import javax.inject.Inject

interface AirVisualDetectionByLocationPresenter : BasePresenter {
    fun loadCityByLocation(
        latitude: Double,
        longitude: Double
    )
}

class AirVisualDetectionByLocationPresenterImpl @Inject constructor(
    private val context: Context,
    private val view: AirVisualCityLocationView,
    private val mapperImpl: AirVisualCityDetailsMapper,
    private val interator: AirVisualCityLocationInteractor
) : BasePresenterImpl(), AirVisualDetectionByLocationPresenter {
    override fun loadCityByLocation(latitude: Double, longitude: Double) {
        compositeDisposable.add(interator.loadCityByLocation(
            latitude = latitude,
            longitude = longitude
        )
            .map { mapperImpl.mapToViewState(it) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(::handleSuccess, ::handleError))
    }

    private fun handleSuccess(model: AirVisualCityDetailsViewState) {
        view.hideProgress()
        view.showCity(context.getString(R.string.city_country, model.city, model.country))
        view.showPollution(context.getString(R.string.aqicn_value, model.pollution.aqicn))
    }

    private fun handleError(e: Throwable) {}
}