package ru.portfolio.airvisual.presentation.detection_city_by_ip.view

import android.os.Bundle
import android.view.View
import ru.portfolio.airvisual.presentation.detection_city.view.BaseCityDetectionIndicatorFragment
import ru.portfolio.airvisual.presentation.detection_city_by_ip.presenter.AirVisualCityDetailsByIpPresenter
import javax.inject.Inject

class AirVisualDetectionByIpFragment : BaseCityDetectionIndicatorFragment() {

    @Inject
    lateinit var presenter: AirVisualCityDetailsByIpPresenter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.loadCityDetailsByIp()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.clear()
    }
}