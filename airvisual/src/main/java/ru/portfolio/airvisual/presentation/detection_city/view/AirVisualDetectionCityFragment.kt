package ru.portfolio.airvisual.presentation.detection_city.view

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter
import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegateLayoutContainer
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_detection_city.*
import kotlinx.android.synthetic.main.item_list_detection_element.view.*
import ru.portfolio.airvisual.R
import ru.portfolio.airvisual.domain.city_detection_storage.CityDetectionModel
import ru.portfolio.airvisual.presentation.city.view.ARGUMENT_VALUE_STATE
import ru.portfolio.airvisual.presentation.city_details.view.ARGUMENT_VALUE_CITY
import ru.portfolio.airvisual.presentation.detection_city.presenter.AirCityDetectionPresenter
import ru.portfolio.airvisual.presentation.state.view.ARGUMENT_VALUE_COUNTRY
import ru.portfolio.core.navigation.NavigationController
import ru.portfolio.core.navigation.NavigationType
import javax.inject.Inject

interface AirVisualDetectionCityView {
    fun showSavedList(list: List<CityDetectionModel>)
    fun hideEmptyPlace()
}

class AirVisualDetectionCityFragment : DaggerFragment(), AirVisualDetectionCityView {

    @Inject
    lateinit var presenter: AirCityDetectionPresenter
    private var navigationController: NavigationController? = null
    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is NavigationController) navigationController = context
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_detection_city, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.getSavedCities()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        presenter.clear()
    }

    override fun showSavedList(list: List<CityDetectionModel>) {
        val adapter = ListDelegationAdapter<List<CityDetectionModel>>(
            elementAdapterDelegate {
                navigationController?.navigate(NavigationType.CITY_DETAILS, Bundle().apply {
                    putString(ARGUMENT_VALUE_COUNTRY, it.country)
                    putString(ARGUMENT_VALUE_STATE, it.state)
                    putString(ARGUMENT_VALUE_CITY, it.city)
                })
            }
        )
        adapter.items = list
        list_detection.layoutManager = LinearLayoutManager(context)
        list_detection.adapter = adapter
    }

    override fun hideEmptyPlace() {
        place_empty.visibility = View.GONE
    }

    private inline fun elementAdapterDelegate(crossinline itemClickedListener: (CityDetectionModel) -> Unit) =
        adapterDelegateLayoutContainer<CityDetectionModel, CityDetectionModel>(R.layout.item_list_detection_element) {
            itemView.setOnClickListener { itemClickedListener(item) }
            bind {
                itemView.element.text = item.city
                itemView.country.text = item.country
                itemView.state.text = item.state
            }
        }
}