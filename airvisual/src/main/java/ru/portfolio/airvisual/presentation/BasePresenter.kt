package ru.portfolio.airvisual.presentation

import io.reactivex.disposables.CompositeDisposable

interface BasePresenter {
    fun clear()
}

abstract class BasePresenterImpl : BasePresenter {
    protected val compositeDisposable = CompositeDisposable()

    override fun clear() {
        compositeDisposable.dispose()
        compositeDisposable.clear()
    }
}