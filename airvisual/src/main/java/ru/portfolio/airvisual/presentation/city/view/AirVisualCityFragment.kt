package ru.portfolio.airvisual.presentation.city.view

import android.os.Bundle
import android.view.View
import ru.portfolio.airvisual.presentation.base.view.AirVisualListFragment
import ru.portfolio.airvisual.presentation.city.presenter.AirVisualCityPresenter
import ru.portfolio.airvisual.presentation.city_details.view.ARGUMENT_VALUE_CITY
import ru.portfolio.airvisual.presentation.state.view.ARGUMENT_VALUE_COUNTRY
import ru.portfolio.core.navigation.NavigationType
import javax.inject.Inject

const val ARGUMENT_VALUE_STATE = "argument_value_state"

class AirVisualCityFragment : AirVisualListFragment() {

    @Inject
    lateinit var presenter: AirVisualCityPresenter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.loadCityList(state = pullStateFromArg(), country = pullCountryFromArg())
    }

    override fun nextStep(value: String) {
        navigationController?.navigate(
            NavigationType.CITY_DETAILS,
            Bundle().apply {
                putString(ARGUMENT_VALUE_COUNTRY, pullCountryFromArg())
                putString(ARGUMENT_VALUE_STATE, pullStateFromArg())
                putString(ARGUMENT_VALUE_CITY, value)
            })
    }

    override fun search(search: String?) {
        presenter.search(pullCountryFromArg(), pullStateFromArg(), search)
    }

    private fun pullStateFromArg() = arguments?.getString(ARGUMENT_VALUE_STATE) ?: ""
    private fun pullCountryFromArg() = arguments?.getString(ARGUMENT_VALUE_COUNTRY) ?: ""
}


