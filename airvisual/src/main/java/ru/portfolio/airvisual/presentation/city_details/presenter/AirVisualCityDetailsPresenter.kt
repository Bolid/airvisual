package ru.portfolio.airvisual.presentation.city_details.presenter

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import ru.portfolio.airvisual.domain.city_details.AirVisualCityDetailsInteractor
import ru.portfolio.airvisual.domain.city_detection_storage.CityDetectionSaveInteractor
import ru.portfolio.airvisual.presentation.city_details.view.AirVisualCityDetailsView
import ru.portfolio.airvisual.presentation.city_details.view.AirVisualCityDetailsViewState
import javax.inject.Inject

interface AirVisualCityDetailsPresenter {
    fun loadCityDetails(country: String, state: String, city: String)
    fun saveForDetection(country: String, state: String, city: String)
    fun combineForShare(model: AirVisualCityDetailsViewState)
    fun clear()
}

class AirVisualCityDetailsPresenterImpl @Inject constructor(
    private val view: AirVisualCityDetailsView,
    private val interactorImpl: AirVisualCityDetailsInteractor,
    private val cityDetectionSaveInteractor: CityDetectionSaveInteractor,
    private val mapper: AirVisualCityDetailsMapper
) : AirVisualCityDetailsPresenter {
    private val compositeDisposable = CompositeDisposable()
    override fun loadCityDetails(country: String, state: String, city: String) {
        compositeDisposable.add(interactorImpl.loadCityDetailsList(country, state, city)
            .map { mapper.mapToViewState(it) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(::handleSuccess, ::handleError))
    }

    override fun saveForDetection(country: String, state: String, city: String) {
        cityDetectionSaveInteractor.saveCityForDetection(country, state, city)
    }

    override fun combineForShare(model: AirVisualCityDetailsViewState) {
        val list = arrayListOf(model.city, model.country, model.state, model.pollution.aqicn)
        val result = list.joinToString(",")
        view.shareStr(result)
    }

    override fun clear() {
        compositeDisposable.dispose()
        compositeDisposable.clear()
    }

    private fun handleSuccess(cityDetails: AirVisualCityDetailsViewState) {
        view.showWeather(cityDetails.weather)
        view.showPollution(cityDetails.pollution)
        view.sharingActivate(cityDetails)
    }

    private fun handleError(t: Throwable) {

    }

}