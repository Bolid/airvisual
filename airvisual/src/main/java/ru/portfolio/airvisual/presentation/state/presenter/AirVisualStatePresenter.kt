package ru.portfolio.airvisual.presentation.state.presenter

import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.portfolio.airvisual.domain.state.AirVisualStateInteractor
import ru.portfolio.airvisual.presentation.base.presenter.AirVisualBasePresenterImpl
import ru.portfolio.airvisual.presentation.base.view.AirVisualView
import javax.inject.Inject

interface AirVisualStatePresenter {
    fun loadStateList(country: String)
    fun search(country: String, search: String? = null)
}

class AirVisualStatePresenterImpl @Inject constructor(
    view: AirVisualView,
    private val interactor: AirVisualStateInteractor
) : AirVisualBasePresenterImpl(view),
    AirVisualStatePresenter {

    override fun search(country: String, search: String?) {
        compositeDisposable.add(
            getDataThread(country, search)
                .subscribe(::handleSearchingData, ::handleError)
        )
    }

    override fun loadStateList(country: String) {
        compositeDisposable.add(
            getDataThread(country)
                .subscribe(::handleSuccess, ::handleError)
        )
    }

    private fun getDataThread(country: String, search: String? = null): Single<List<String>> {
        return interactor.loadStateList(country, search)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }
}
