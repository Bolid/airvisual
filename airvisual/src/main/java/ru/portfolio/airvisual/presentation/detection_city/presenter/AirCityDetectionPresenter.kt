package ru.portfolio.airvisual.presentation.detection_city.presenter

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.portfolio.airvisual.domain.city_detection_storage.CityDetectionModel
import ru.portfolio.airvisual.domain.city_detection_storage.CityDetectionReadInteractor
import ru.portfolio.airvisual.presentation.BasePresenter
import ru.portfolio.airvisual.presentation.BasePresenterImpl
import ru.portfolio.airvisual.presentation.detection_city.view.AirVisualDetectionCityView
import javax.inject.Inject

interface AirCityDetectionPresenter : BasePresenter {
    fun getSavedCities()
}

class AirCityDetectionPresenterImpl @Inject constructor(
    private val view: AirVisualDetectionCityView,
    private val interactor: CityDetectionReadInteractor
) : BasePresenterImpl(), AirCityDetectionPresenter {

    override fun getSavedCities() {
        compositeDisposable.add(
            interactor.readCityDetection()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(::handleSuccess)
        )
    }

    private fun handleSuccess(list: List<CityDetectionModel>?) {
        if (list != null) {
            view.showSavedList(list)
            view.hideEmptyPlace()
        }
    }
}