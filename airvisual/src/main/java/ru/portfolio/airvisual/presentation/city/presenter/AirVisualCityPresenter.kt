package ru.portfolio.airvisual.presentation.city.presenter

import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.portfolio.airvisual.domain.city.AirVisualCityInteractor
import ru.portfolio.airvisual.presentation.base.presenter.AirVisualBasePresenter
import ru.portfolio.airvisual.presentation.base.presenter.AirVisualBasePresenterImpl
import ru.portfolio.airvisual.presentation.base.view.AirVisualView
import javax.inject.Inject

interface AirVisualCityPresenter : AirVisualBasePresenter {
    fun loadCityList(state: String, country: String)
    fun search(state: String, country: String, search: String? = null)
}

class AirVisualCityPresenterImpl @Inject constructor(
    view: AirVisualView,
    private val interactor: AirVisualCityInteractor
) : AirVisualBasePresenterImpl(view),
    AirVisualCityPresenter {

    override fun search(state: String, country: String, search: String?) {
        compositeDisposable.add(
            getThreadData(state, country, search)
                .subscribe(::handleSearchingData, ::handleError)
        )
    }

    override fun loadCityList(state: String, country: String) {
        compositeDisposable.add(
            getThreadData(country, state)
                .subscribe(::handleSuccess, ::handleError)
        )
    }

    private fun getThreadData(
        country: String,
        state: String,
        search: String? = null
    ): Single<List<String>> {
        return interactor.loadCityList(country = country, state = state, search = search)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }
}
