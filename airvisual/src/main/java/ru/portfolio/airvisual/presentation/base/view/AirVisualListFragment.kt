package ru.portfolio.airvisual.presentation.base.view

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter
import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegateLayoutContainer
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_country_list.*
import kotlinx.android.synthetic.main.item_list_element.view.*
import ru.portfolio.SearchChangeListener
import ru.portfolio.airvisual.R
import ru.portfolio.core.navigation.NavigationController

interface AirVisualView {
    fun showResultList(listElement: List<String>)
    fun showSearchingData(listElement: List<String>)
    fun showError()
}

abstract class AirVisualListFragment : DaggerFragment(),
    AirVisualView {

    protected var navigationController: NavigationController? = null
    private var searchChangeListener: SearchChangeListener? = null

    abstract fun nextStep(value: String)
    abstract fun search(search: String?)
    private lateinit var adapter: ListDelegationAdapter<List<String>>
    final override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is NavigationController) navigationController = context
        if (context is SearchChangeListener) searchChangeListener = context
    }

    final override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        searchChangeListener?.onSearchChange(::search)
        return inflater.inflate(R.layout.fragment_country_list, container, false)
    }

    override fun showResultList(listElement: List<String>) {
        adapter = ListDelegationAdapter<List<String>>(
            elementAdapterDelegate { nextStep(it) }
        )
        adapter.items = listElement
        list.layoutManager = LinearLayoutManager(context)
        list.adapter = adapter
    }

    override fun showSearchingData(listElement: List<String>) {
        adapter.items = listElement
        adapter.notifyDataSetChanged()
    }

    override fun showError() {
        Toast.makeText(context, "Error", Toast.LENGTH_LONG).show()
    }

    private inline fun elementAdapterDelegate(crossinline itemClickedListener: (String) -> Unit) =
        adapterDelegateLayoutContainer<String, String>(R.layout.item_list_element) {
            itemView.element.setOnClickListener { itemClickedListener(item) }
            bind {
                itemView.element.text = item
            }
        }
}