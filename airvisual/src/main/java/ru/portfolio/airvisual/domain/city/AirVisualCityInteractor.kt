package ru.portfolio.airvisual.domain.city

import io.reactivex.Single
import ru.portfolio.airvisual.data.city.AirVisualCityRepository
import javax.inject.Inject

interface AirVisualCityInteractor {
    fun loadCityList(country: String, state: String, search: String?): Single<List<String>>
}

class AirVisualCityInteractorImpl @Inject constructor(private val repositoryAirVisual: AirVisualCityRepository) :
    AirVisualCityInteractor {

    override fun loadCityList(
        country: String,
        state: String,
        search: String?
    ): Single<List<String>> {
        return repositoryAirVisual.loadCityList(country = country, state = state, search = search)
    }
}