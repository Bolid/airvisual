package ru.portfolio.airvisual.domain.city_detection_storage

import io.reactivex.Single
import ru.portfolio.airvisual.data.city_detection_storage.CityDetectionReadRepository
import ru.portfolio.airvisual.data.city_detection_storage.CityDetectionSaveRepository
import javax.inject.Inject

interface CityDetectionSaveInteractor {
    fun saveCityForDetection(country: String, state: String, city: String)
}

interface CityDetectionReadInteractor {
    fun readCityDetection(): Single<List<CityDetectionModel>?>
}

class CityDetectionSaveInteractorImpl @Inject constructor(private val saveRepository: CityDetectionSaveRepository) :
    CityDetectionSaveInteractor {
    override fun saveCityForDetection(country: String, state: String, city: String) {
        saveRepository.saveCity(CityDetectionModel(country, state, city))
    }
}

class CityDetectionReadInteractorImpl @Inject constructor(private val repository: CityDetectionReadRepository) :
    CityDetectionReadInteractor {
    override fun readCityDetection(): Single<List<CityDetectionModel>?> {
        return Single.create { emitter -> repository.getSavedCity()?.let { emitter.onSuccess(it) } }
    }
}


data class CityDetectionModel(
    val country: String,
    val state: String,
    val city: String
)