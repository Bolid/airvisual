package ru.portfolio.airvisual.domain.city_detection_location

import io.reactivex.Single
import ru.portfolio.airvisual.data.city_by_location.AirVisualCityDetailsLocationRepository
import ru.portfolio.airvisual.data.city_details.CityDetailsModel
import javax.inject.Inject

interface AirVisualCityLocationInteractor {
    fun loadCityByLocation(latitude: Double, longitude: Double): Single<CityDetailsModel>
}

class AirVisualCityLocationInteractorImpl @Inject constructor(private val repositoryDetails: AirVisualCityDetailsLocationRepository) :
    AirVisualCityLocationInteractor {
    override fun loadCityByLocation(latitude: Double, longitude: Double): Single<CityDetailsModel> {
        return repositoryDetails.loadCityDetails(latitude = latitude, longitude = longitude)
    }
}