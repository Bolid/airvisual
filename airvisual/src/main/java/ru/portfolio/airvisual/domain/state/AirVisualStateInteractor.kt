package ru.portfolio.airvisual.domain.state

import io.reactivex.Single
import ru.portfolio.airvisual.data.state.AirVisualStateRepository
import javax.inject.Inject

interface AirVisualStateInteractor {
    fun loadStateList(country: String, search: String?): Single<List<String>>
}

class AirVisualAirVisualStateInteractorImpl @Inject constructor(private val repository: AirVisualStateRepository) :
    AirVisualStateInteractor {
    override fun loadStateList(country: String, search: String?): Single<List<String>> {
        return repository.loadStateList(country, search)
    }
}