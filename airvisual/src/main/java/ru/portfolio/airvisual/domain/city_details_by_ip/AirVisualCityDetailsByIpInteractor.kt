package ru.portfolio.airvisual.domain.city_details_by_ip

import io.reactivex.Single
import ru.portfolio.airvisual.data.city_by_ip.AirVisualCityDetailsIpRepository
import ru.portfolio.airvisual.data.city_details.CityDetailsModel
import javax.inject.Inject

interface AirVisualCityDetailsByIpInteractor {
    fun loadCityByLocation(): Single<CityDetailsModel>
}

class AirVisualCityDetailsByIpInteractorImpl @Inject constructor(private val repositoryDetails: AirVisualCityDetailsIpRepository) :
    AirVisualCityDetailsByIpInteractor {
    override fun loadCityByLocation(): Single<CityDetailsModel> {
        return repositoryDetails.loadCityDetails()
    }
}