package ru.portfolio.airvisual.domain.country

import io.reactivex.Single
import ru.portfolio.airvisual.data.country.AirVisualCountryRepository
import javax.inject.Inject

interface AirVisualCountryInteractor {
    fun loadCountryList(search: String?): Single<List<String>>
}

class AirVisualCountryInteractorImpl @Inject constructor(private val repositoryAirVisual: AirVisualCountryRepository) :
    AirVisualCountryInteractor {
    override fun loadCountryList(search: String?): Single<List<String>> {
        return repositoryAirVisual.loadCountryList(search)
    }
}