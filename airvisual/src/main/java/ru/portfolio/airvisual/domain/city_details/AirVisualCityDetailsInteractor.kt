package ru.portfolio.airvisual.domain.city_details

import io.reactivex.Single
import ru.portfolio.airvisual.data.city_details.AirVisualCityDetailsRepository
import ru.portfolio.airvisual.data.city_details.CityDetailsModel
import javax.inject.Inject

interface AirVisualCityDetailsInteractor {
    fun loadCityDetailsList(country: String, state: String, city: String): Single<CityDetailsModel>
}

class AirVisualCityDetailsInteractorImpl @Inject constructor(private val repositoryAirVisual: AirVisualCityDetailsRepository) :
    AirVisualCityDetailsInteractor {
    override fun loadCityDetailsList(
        country: String,
        state: String,
        city: String
    ): Single<CityDetailsModel> {
        return repositoryAirVisual.loadCityDetailsList(
            country = country,
            state = state,
            city = city
        )
    }
}