package ru.portfolio.airvisual.repository.city_details_by_ip

import io.reactivex.Single
import ru.portfolio.airvisual.BuildConfig
import ru.portfolio.airvisual.data.api.AirVisualApi
import ru.portfolio.airvisual.data.city_by_ip.AirVisualCityDetailsIpRepository
import ru.portfolio.airvisual.data.city_details.CityDetailsModel
import ru.portfolio.airvisual.data.city_details.CityDetailsModelMapper
import javax.inject.Inject

class AirVisualCityDetailsIpRepositoryImpl @Inject constructor(
    private val api: AirVisualApi,
    private val mapper: CityDetailsModelMapper
) :
    AirVisualCityDetailsIpRepository {
    override fun loadCityDetails(): Single<CityDetailsModel> {
        return api.getCityDetailsByIp(BuildConfig.KEY_AIR_VISUAL)
            .map {
                mapper.mapToCityDetailsModel(it)
            }
    }
}