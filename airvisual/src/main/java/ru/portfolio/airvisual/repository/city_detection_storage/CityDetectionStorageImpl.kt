package ru.portfolio.airvisual.repository.city_detection_storage

import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import ru.portfolio.airvisual.data.city_detection_storage.CityDetectionReadRepository
import ru.portfolio.airvisual.data.city_detection_storage.CityDetectionSaveRepository
import ru.portfolio.airvisual.domain.city_detection_storage.CityDetectionModel
import javax.inject.Inject

private const val KEY_PREFERENCE_DETECTION_CITY = "key_preference_detection_city"

class CityDetectionSharedSaveRepositoryImpl @Inject constructor(
    private val preferences: SharedPreferences,
    private val converterData: ConverterData<CityDetectionModel>
) :
    CityDetectionSaveRepository, CityDetectionReadRepository {

    override fun saveCity(model: CityDetectionModel) {
        var list = getSavedCity()
        if (list?.contains(model) == true) return
        if (list == null) list = arrayListOf()
        if (list is ArrayList<CityDetectionModel>) list.add(model)
        val str = converterData.convertDataToString(list)
        preferences
            .edit()
            .putString(KEY_PREFERENCE_DETECTION_CITY, str)
            .apply()
    }

    override fun getSavedCity(): List<CityDetectionModel>? {
        val str = preferences.getString(KEY_PREFERENCE_DETECTION_CITY, "") ?: ""
        return converterData.convertToData(str)
    }

}

interface ConverterData<T> {
    fun convertDataToString(model: List<T>): String
    fun convertToData(str: String): List<T>?
}

class GsonConverterData @Inject constructor() : ConverterData<CityDetectionModel> {
    override fun convertDataToString(model: List<CityDetectionModel>): String {
        return Gson().toJson(model)
    }

    override fun convertToData(str: String): List<CityDetectionModel>? {
        val type = object : TypeToken<List<CityDetectionModel>>() {}.type
        return Gson().fromJson(str, type)
    }
}