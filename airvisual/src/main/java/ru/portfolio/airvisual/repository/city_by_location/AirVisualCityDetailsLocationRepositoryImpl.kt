package ru.portfolio.airvisual.repository.city_by_location

import io.reactivex.Single
import ru.portfolio.airvisual.BuildConfig
import ru.portfolio.airvisual.data.api.AirVisualApi
import ru.portfolio.airvisual.data.city_by_location.AirVisualCityDetailsLocationRepository
import ru.portfolio.airvisual.data.city_details.CityDetailsModel
import ru.portfolio.airvisual.data.city_details.CityDetailsModelMapper
import javax.inject.Inject

class AirVisualCityDetailsLocationRepositoryImpl @Inject constructor(
    private val api: AirVisualApi,
    private val mapper: CityDetailsModelMapper
) :
    AirVisualCityDetailsLocationRepository {
    override fun loadCityDetails(latitude: Double, longitude: Double): Single<CityDetailsModel> {
        return api.getCityDetailsByLocation(
            key = BuildConfig.KEY_AIR_VISUAL,
            lat = latitude,
            lon = longitude
        ).map {
            mapper.mapToCityDetailsModel(it)
        }
    }
}