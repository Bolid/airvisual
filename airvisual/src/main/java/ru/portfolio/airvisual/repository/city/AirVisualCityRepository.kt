package ru.portfolio.airvisual.repository.city

import io.reactivex.Single
import ru.portfolio.airvisual.BuildConfig
import ru.portfolio.airvisual.data.api.AirVisualApi
import ru.portfolio.airvisual.data.api.CityResponse
import ru.portfolio.airvisual.data.city.AirVisualCityRepository
import ru.portfolio.airvisual.repository.cache.CacheRepositoryImpl
import javax.inject.Inject

class AirVisualCityRepositoryImpl @Inject constructor(private val api: AirVisualApi) :
    CacheRepositoryImpl(),
    AirVisualCityRepository {
    override fun loadCityList(
        country: String,
        state: String,
        search: String?
    ): Single<List<String>> {
        if (cacheList != null) return Single.create { emitter ->
            emitter.onSuccess(cacheList!!.filter {
                it.contains(
                    search ?: ""
                )
            })
        }
        return api.getCitiesList(key = BuildConfig.KEY_AIR_VISUAL, state = state, country = country)
            .map {
                cacheList = mapToStringList(it.data)
                cacheList
            }
    }


    private fun mapToStringList(listCities: List<CityResponse>): List<String> {
        return listCities.map { it.city }
    }
}