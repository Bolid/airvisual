package ru.portfolio.airvisual.repository.country

import io.reactivex.Single
import ru.portfolio.airvisual.BuildConfig
import ru.portfolio.airvisual.data.api.AirVisualApi
import ru.portfolio.airvisual.data.api.CountryResponse
import ru.portfolio.airvisual.data.country.AirVisualCountryRepository
import ru.portfolio.airvisual.repository.cache.CacheRepositoryImpl
import javax.inject.Inject

class AirVisualCountryRepositoryImpl @Inject constructor(
    private val api: AirVisualApi
) : CacheRepositoryImpl(), AirVisualCountryRepository {

    override fun loadCountryList(search: String?): Single<List<String>> {
        if (cacheList != null) return Single.create { emitter ->
            emitter.onSuccess(cacheList!!.filter { it.contains(search ?: "") })
        }
        return api.getCountryList(key = BuildConfig.KEY_AIR_VISUAL).map {
            val list = mapToStringList(it.data)
            cacheList = list
            list
        }
    }

    private fun mapToStringList(listCounty: List<CountryResponse>): List<String> {
        return listCounty.map { it.country }
    }

}