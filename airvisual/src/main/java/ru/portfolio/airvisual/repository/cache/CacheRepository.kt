package ru.portfolio.airvisual.repository.cache

interface CacheRepository<T> {
    fun getCacheValues(): List<T>?
}

open class CacheRepositoryImpl : CacheRepository<String> {

    protected var cacheList: List<String>? = null

    override fun getCacheValues(): List<String>? {
        return cacheList
    }
}