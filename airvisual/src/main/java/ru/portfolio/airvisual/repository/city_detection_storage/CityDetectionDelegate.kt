package ru.portfolio.airvisual.repository.city_detection_storage

import android.os.Build
import androidx.annotation.RequiresApi
import ru.portfolio.airvisual.R
import ru.portfolio.airvisual.data.city_detection_storage.CityDetectionReadRepository
import ru.portfolio.airvisual.data.city_detection_storage.CityDetectionSaveRepository
import ru.portfolio.airvisual.domain.city_detection_storage.CityDetectionModel
import ru.portfolio.core.short_cut_helper.ShortCutHelper
import ru.portfolio.core.short_cut_helper.ShortCutModel
import javax.inject.Inject


class CityDetectionDelegate @Inject constructor(
    private val observer: ObservableDetection<CityDetectionModel>,
    private val repo: CityDetectionSharedSaveRepositoryImpl
) :
    CityDetectionSaveRepository by repo, CityDetectionReadRepository by repo {

    override fun saveCity(model: CityDetectionModel) {
        repo.saveCity(model)
        getSavedCity()?.let { observer.observe(it) }
    }
}

interface ObservableDetection<T> {
    fun observe(model: List<T>)
}

class ObservableCityDetection @Inject constructor(private val shortCutHelper: ShortCutHelper) :
    ObservableDetection<CityDetectionModel> {
    @RequiresApi(Build.VERSION_CODES.N_MR1)
    override fun observe(model: List<CityDetectionModel>) {
        shortCutHelper.shortCutCreate(model.map {
            ShortCutModel(
                it.city,
                it.city,
                R.drawable.ic_baseline_location_city_24
            )
        })
    }
}