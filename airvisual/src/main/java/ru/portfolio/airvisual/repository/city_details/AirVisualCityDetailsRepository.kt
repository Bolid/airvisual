package ru.portfolio.airvisual.repository.city_details

import io.reactivex.Single
import ru.portfolio.airvisual.BuildConfig
import ru.portfolio.airvisual.data.api.AirVisualApi
import ru.portfolio.airvisual.data.city_details.AirVisualCityDetailsRepository
import ru.portfolio.airvisual.data.city_details.CityDetailsModel
import ru.portfolio.airvisual.data.city_details.CityDetailsModelMapper
import javax.inject.Inject

class AirVisualCityDetailsRepositoryImpl @Inject constructor(
    private val api: AirVisualApi,
    private val mapper: CityDetailsModelMapper
) :
    AirVisualCityDetailsRepository {
    override fun loadCityDetailsList(
        country: String,
        state: String,
        city: String
    ): Single<CityDetailsModel> {
        return api.getCityDetails(
            key = BuildConfig.KEY_AIR_VISUAL,
            country = country,
            state = state,
            city = city
        )
            .map {
                mapper.mapToCityDetailsModel(it)
            }
    }
}