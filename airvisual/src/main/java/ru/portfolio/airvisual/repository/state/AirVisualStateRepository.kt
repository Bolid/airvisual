package ru.portfolio.airvisual.repository.state

import io.reactivex.Single
import ru.portfolio.airvisual.BuildConfig
import ru.portfolio.airvisual.data.api.AirVisualApi
import ru.portfolio.airvisual.data.api.StateResponse
import ru.portfolio.airvisual.data.state.AirVisualStateRepository
import ru.portfolio.airvisual.repository.cache.CacheRepositoryImpl
import javax.inject.Inject

class AirVisualStateRepositoryImpl @Inject constructor(
    private val api: AirVisualApi
) : CacheRepositoryImpl(), AirVisualStateRepository {

    override fun loadStateList(country: String, search: String?): Single<List<String>> {
        if (cacheList != null) return Single.create { emitter ->
            emitter.onSuccess(cacheList!!.filter { it.contains(search ?: "") })
        }
        return api.getStateList(key = BuildConfig.KEY_AIR_VISUAL, country = country)
            .map {
                cacheList = mapToStringList(it.data)
                cacheList
            }
    }

    private fun mapToStringList(listState: List<StateResponse>): List<String> {
        return listState.map { it.state }
    }

}